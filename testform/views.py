
from .forms import SarinaForm

def sarina(request):
    form = SarinaForm()
    return render(request, 'sarina.html', {'form': form})
