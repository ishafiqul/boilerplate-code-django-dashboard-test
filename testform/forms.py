from django import forms

class SarinaForm(forms.Form):
    yourname = forms.CharField(max_length=100, label='Your Name')
    message = forms.CharField(widget=forms.Textarea)
